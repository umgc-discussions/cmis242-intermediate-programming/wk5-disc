public interface Cat {
    void eat(String food);
    void sleep();
    void hiss();

}
