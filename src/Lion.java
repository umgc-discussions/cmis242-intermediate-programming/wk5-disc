import java.io.*;
import java.util.*;

public class Lion implements Cat{


    public Lion() {
    }

    public void eat(String food){
        if (food == "Meat") {
            System.out.println("Yum");
            sleep();
        }
        else {
            hiss();
            System.out.println("I don't eat that garbage!");
        }
    }
    public void hiss() {
        System.out.println("HISSSS!");
    }
   public void sleep() {
        System.out.println("YAWN, that was good, I'm going to lay down and sleep!");
    }



   public static void main(String[] args) throws IOException {
        String input;
        System.out.println("Welcome to the Lion Simulator! What do you want to feed your lion?");
        Scanner sc = new Scanner(System.in);
        input = sc.nextLine();
        Lion lion1 = new Lion();
        lion1.eat(input);


    }
}
