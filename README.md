Abstraction is the basis for good Object-Oriented design that is modular, reusable, and maintainable. 

Interfaces and Abstract classes are two mechanisms in Java that provide high-level abstractions.  An interface or abstract class is something which is not concrete, something which is incomplete. 

For this discussion, what do you think is meant by “Programming to an interface”?  What are some differences between Abstract Classes and Interfaces?  Provide a simple example application with an interface definition and a class which implements the interface. 


Alright, programming to an interface. I put some thought into this, and also read a bunch of articles about other people's thoughts on this, and I've seen many different opinions on what this means. 

My opinion on what this means, based on what I've gathered from my research, is that this is a conceptual method of thinking about your code. It basically means to think about WHAT your code is supposed to do, rather than on HOW your code is doing it. This does a number of things for the programmer, the first being that it makes your code flexible. It is easy to change an implementation of your code if you understand what the code is supposed to do (for example, changing your code from a GUI implementation to a console implementation). 

It also makes it easy for other programmers to come in and make changes or improve your code, because an interface is like a contract. It defines variables that any class which uses that interface may have, as well as abstract methods on what that interface can do. The best example of this that I have seen is from the Java Tutorials, where they show a possible interface for a self driving car. Every car company would offer that same interface, but HOW they implement that interface is completely up to them. All other companies would need to know is that if they use the rightTurnBlinker() method, the right turn blinker turns on; they don't need to understand HOW that blinker turns on.

And the difference between an interface and abstract class, straight from the Java documentation, is that all interfaces must be abstract and may not have implementations, where an abstract class can have instance methods that implement a default behavior. All variables declared in an interface are final, whereas an abstract class may have non-final variables. These are just some of the differences stated in the Java documentation. 

My program this week is super simple. It uses a lion class which implements a Cat interface. There is an intentional bug in it, see if you can find it! =)

-Steve